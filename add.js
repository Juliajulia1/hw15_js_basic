/* Теоритичні питання:
1. В чому відмінність між setInterval та setTimeout?
2. Як припинити виконання функції, яка була запланована для виклику з використанням setTimeout та setInterval?

Практичне завдання 1:

-Створіть HTML-файл із кнопкою та елементом div.
-При натисканні кнопки використовуйте setTimeout, щоб змінити текстовий вміст елемента div через затримку 3 секунди. Новий текст повинен вказувати,
 що операція виконана успішно.

Практичне завдання 2:

Реалізуйте таймер зворотного відліку, використовуючи setInterval. При завантаженні сторінки виведіть зворотний відлік від 10 до 1 в елементі div.
Після досягнення 1 змініть текст на "Зворотній відлік завершено".
*/




    document.getElementById("myButton").addEventListener("click", function() {
    let divElement = document.getElementById("myDiv");
    divElement.textContent = "Операція виконується...";

    setTimeout(function() {
    divElement.textContent = "Операція виконана успішно";
}, 3000);
});


let countdownElement = document.getElementById("countdown");
let count = 10;

let countdownInterval = setInterval(function() {
    count--;
    countdownElement.textContent = count;

    if (count === 0) {
        clearInterval(countdownInterval);
        countdownElement.textContent = "Зворотній відлік завершено";
    }
}, 1000);


